# translation of drakconf.po to Arabic
# Mohammed Gamal <f2c2001@yahoo.com>, 2001.
# Boujaj Mostapha Ibrahim <informatik.gelsenkirchen@arcor.de>, 2002.
# Youcef Rabah Rahal <rahal@arabeyes.org>, 2004, 2005.
# Ossama M. Khayat <okhayat@yahoo.com>, 2004, 2005.
# Abdulaziz Al-Arfaj <alarfaj@arabeyes.org>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: drakconf\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-06-18 00:01+0800\n"
"PO-Revision-Date: 2005-09-07 00:38+0300\n"
"Last-Translator: Ossama M. Khayat <okhayat@yahoo.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.10.2\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 0 : n==2 ? 1 : n>=3 && n<=10 ? 2 : 3\n"

#: ../contributors.pl:11
#, c-format
msgid "Packagers"
msgstr "منشؤو الحزم"

#: ../contributors.pl:12 ../contributors.pl:40
#, c-format
msgid "Per Oyvind Karlsen"
msgstr "Per Oyvind Karlsen"

#: ../contributors.pl:12
#, c-format
msgid ""
"massive packages rebuilding and cleaning, games, sparc port, proofreading of "
"Mandriva tools"
msgstr ""
"عمل على إعادة بناء ضخمة للحزم وتنقيتها، والألعاب، والنقل إلى sparc، وتحسين "
"كتابة نصوص أدوات ماندريبا "

#: ../contributors.pl:13
#, c-format
msgid "Guillaume Rousse"
msgstr "Guillaume Rousse"

#: ../contributors.pl:13
#, c-format
msgid "cowsay introduction"
msgstr "مقدّمة cowsay"

#: ../contributors.pl:14
#, c-format
msgid "Olivier Thauvin"
msgstr "Olivier Thauvin"

#: ../contributors.pl:14
#, c-format
msgid "figlet introduction, Distriblint (checking rpm in the distro)"
msgstr "مقدّمة figlet. Distriblint )فحص حزم rpm في التوزيعة)"

#: ../contributors.pl:15
#, c-format
msgid "Marcel Pol"
msgstr "Marcel Pol"

#: ../contributors.pl:15
#, c-format
msgid "xfce4, updated abiword, mono"
msgstr "xfce4، abiword مُحدّث، mono"

#: ../contributors.pl:16
#, c-format
msgid "Ben Reser"
msgstr "Ben Reser"

#: ../contributors.pl:16
#, c-format
msgid ""
"updated nc with debian patches, fixed some perl packages, dnotify startup "
"script, urpmc, hddtemp, wipe, etc..."
msgstr ""
"حدّث nc بتصحيحات ديبيان، صحّح بعض حزم بيرل، برنامج dnotify startup، urpmc، "
"hddtemp، wipe، إلخ..."

#: ../contributors.pl:17 ../contributors.pl:42
#, c-format
msgid "Thomas Backlund"
msgstr "Thomas Backlund"

#: ../contributors.pl:17
#, c-format
msgid ""
"\"deep and broad\" kernel work (many new patches before integration in "
"official kernel)"
msgstr ""
"عمل \"عميق و عريض\" على النواة (العديد من التصحيحات الجديدة قبل دمجها في "
"النواة الرسمية)"

#: ../contributors.pl:18
#, c-format
msgid "Svetoslav Slavtchev"
msgstr "Svetoslav Slavtchev"

#: ../contributors.pl:18
#, c-format
msgid "kernel work (audio- and video-related patches)"
msgstr "عمل على النواة (تصحيحات متعلّقة بالصوت والفيديو)"

#: ../contributors.pl:19
#, c-format
msgid "Danny Tholen"
msgstr "Danny Tholen"

#: ../contributors.pl:19
#, c-format
msgid "patches to some packages, kfiresaver, xwine. ppc kernel-benh."
msgstr "رقع لبعض الحزم، kfiresaver، xwine. ppc kernel-benh."

#: ../contributors.pl:20
#, c-format
msgid "Buchan Milne"
msgstr "Buchan Milne"

#: ../contributors.pl:20
#, c-format
msgid ""
"Samba 3.0 (prerelease) that co-exists with Samba 2.2.x, Samba-2.2.x, GIS "
"software (grass, mapserver), cursor_themes collection, misc server-side "
"contributions"
msgstr ""
"Samba 3.0.(قبل إصدارة) و التي تتعايش مع Samba 2.2.x، Samba 2.2.x، برامج GIS "
"(grass، mapserver)، مجموعة سمات المؤشّرات، مساهمات مختلفة من ناحية الخادم"

#: ../contributors.pl:21
#, c-format
msgid "Goetz Waschk"
msgstr "Goetz Waschk"

#: ../contributors.pl:21
#, c-format
msgid ""
"xine, totem, gstreamer, mplayer, vlc, vcdimager, xmms and plugins gnome-"
"python, rox desktop"
msgstr ""
"xine، totem، gstreamer، mplayer، vlc، vcdimager، xmms و إضافات جينوم-بيثون، "
"سطح مكتب rox)"

#: ../contributors.pl:22
#, c-format
msgid "Austin Acton"
msgstr "Austin Acton"

#: ../contributors.pl:22
#, c-format
msgid ""
"audio/video/MIDI apps, scientific apps, audio/video production howtos, "
"bluetooth, pyqt & related"
msgstr ""
"تطبيقات للصوت، الفيديو و MIDI، تطبيقات علمية، دلائل لإنتاج الصوت و الفيديو، "
"bluetooth، pyqt و ما تعلّق بها"

#: ../contributors.pl:23
#, c-format
msgid "Spencer Anderson"
msgstr "Spencer Anderson"

#: ../contributors.pl:23
#, c-format
msgid "ATI/gatos/DRM stuff, opengroupware.org"
msgstr "أشياء متعلّقة بـATI/gatos/DRM، opengroupware.org"

#: ../contributors.pl:24
#, c-format
msgid "Andrey Borzenkov"
msgstr "Andrey Borzenkov"

#: ../contributors.pl:24
#, c-format
msgid "supermount-ng and other kernel work"
msgstr "عمل على supermount-ng و النواة"

#: ../contributors.pl:25
#, c-format
msgid "Oden Eriksson"
msgstr "Oden Eriksson"

#: ../contributors.pl:25
#, c-format
msgid "most web-based packages and many security-related packages"
msgstr "معظم الحزم على الإنترنت والعديد من الحزم المتعلّقة بالأمن"

#: ../contributors.pl:26
#, c-format
msgid "Stefan VanDer Eijk"
msgstr "Stefan VanDer Eijk"

#: ../contributors.pl:26
#, c-format
msgid "slbd distro checking, devel dependancies"
msgstr "فحص slbd في التوزيعة، مُعتمدات devel"

#: ../contributors.pl:27
#, c-format
msgid "David Walser"
msgstr "David Walser"

#: ../contributors.pl:27
#, c-format
msgid "rpmsync script, foolproof MIDI playback, tweaked libao"
msgstr "برنامج rpmsync، عصم إعادة MIDI، ضبط libao"

#: ../contributors.pl:28
#, c-format
msgid "Andi Payn"
msgstr "Andi Payn"

#: ../contributors.pl:28
#, c-format
msgid "many extra gnome applets and python modules"
msgstr "العديد من بريمجات جينوم ووحدات بايثون الإضافية"

#: ../contributors.pl:29 ../contributors.pl:41
#, c-format
msgid "Tibor Pittich"
msgstr "Tibor Pittich"

#: ../contributors.pl:29
#, c-format
msgid ""
"sk-i18n, contributed several packages, openldap testing and integration, "
"bind-sdb-ldap, several years of using cooker and bug hunting, etc..."
msgstr ""
"قائد فريق ماندريبا للتّدويل إلى السّلوفاكية، ساهم بالعديد من الحزم، وباختبار "
"openldap ودمجه، وbind-sdb-ldap، والعديد من السنوات باستخدام نظام cooker "
"وتتبع العيوب البرمجية، إلخ......"

#: ../contributors.pl:30
#, c-format
msgid "Pascal Terjan"
msgstr "Pascal Terjan"

#: ../contributors.pl:30
#, c-format
msgid "some ruby stuff, php-pear packages, various other stuff."
msgstr "بعض الأشياء المتعلّقة بروبي (ruby)، والعديد من الأشياء المختلفة."

#: ../contributors.pl:31
#, c-format
msgid "Michael Reinsch"
msgstr "Michael Reinsch"

#: ../contributors.pl:31
#, c-format
msgid "moin wiki clone, beep-media-player, im-ja and some other packages"
msgstr "شبيه moin wiki، beep-media-player، im-ja وبعض الحزم الأخرى"

#: ../contributors.pl:32
#, c-format
msgid "Christophe Guilloux"
msgstr "Christophe Guilloux"

#: ../contributors.pl:32
#, c-format
msgid "bug reports, help with thunderbird package,..."
msgstr "تقارير الخلل، والمساعدة بحزمة thunderbird،..."

#: ../contributors.pl:33
#, c-format
msgid "Brook Humphrey"
msgstr "Brook Humphrey"

#: ../contributors.pl:33
#, c-format
msgid ""
"testing and bug reports, Dovecot, bibletime, sword, help with pure-ftpd, "
"spamassassin, maildrop, clamav."
msgstr ""
"الاختبار وتقرير الخلل، Dovecot، bibletime، swordK المساعدة في pure-ftpd، "
"spamassassin، maildrop، clamav."

#: ../contributors.pl:34
#, c-format
msgid "Olivier Blin"
msgstr "Olivier Blin"

#: ../contributors.pl:34
#, c-format
msgid ""
"http proxy support in installer, kernel 2.6 support in sndconfig, samba3 "
"support in LinNeighborhood, fixes and enhancements in urpmi, bootsplash and "
"drakxtools"
msgstr ""
"دعم بروكسي http في برنامج التثبيت، دعم النواة 2.6 في sndconfig، دعم Samba3 "
"في LinNeighborhood، إصلاحات وتحسينات في urpmi، شاشة الإقلاع وdrakxtools"

#: ../contributors.pl:35
#, c-format
msgid "Emmanuel Blindauer"
msgstr "Emmanuel Blindauer"

#: ../contributors.pl:35
#, c-format
msgid "lm_sensors for 2.6 kernel, testing, some contrib packages."
msgstr "lm_sensors للنواة 2.6، والاختبار وبعض حزم المشاركة."

#: ../contributors.pl:36
#, c-format
msgid "Matthias Debus"
msgstr "Matthias Debus"

#: ../contributors.pl:36
#, c-format
msgid "sim, pine and some other contrib packages."
msgstr "sim، وpine وبعض حزم المشاركة الأخرى."

#: ../contributors.pl:37
#, c-format
msgid "Documentation"
msgstr "الوثائق"

#: ../contributors.pl:38
#, c-format
msgid "SunnyDubey"
msgstr "SunnyDubey"

#: ../contributors.pl:38
#, c-format
msgid "wrote/edited parts of gi/docs/HACKING file"
msgstr "كتابة/تحرير أجزاء من ملفات gi/docs/HACKING"

#: ../contributors.pl:39
#, c-format
msgid "Translators"
msgstr "المترجمون"

#: ../contributors.pl:40
#, c-format
msgid "Norwegian Bokmal (nb) translator and coordinator, i18n work"
msgstr "مترجم و منسّق للترجمة النرويجية، عمل على التدويل."

#: ../contributors.pl:41
#, c-format
msgid "\"one-man\" mdk sk-i18n team"
msgstr "فريق \"الرجل الواحد\" لتدويل ماندريبا للسلوفاكية"

#: ../contributors.pl:42
#, c-format
msgid "Finnish translator and coordinator"
msgstr "مترجم و منسّق الفنلندية"

#: ../contributors.pl:43
#, c-format
msgid "Reinout Van Schouwen"
msgstr "Reinout Van Schouwen"

#: ../contributors.pl:43
#, c-format
msgid "Dutch translator and coordinator"
msgstr "مترجم و منسّق الهولندية"

#: ../contributors.pl:44
#, c-format
msgid "Keld Simonsen"
msgstr "Keld Simonsen"

#: ../contributors.pl:44
#, c-format
msgid "Danish translator (and some Bokmal too:-)"
msgstr "مترجم و منسّق الفنلندية (وبعض النروجية كذلك)"

#: ../contributors.pl:45
#, c-format
msgid "Karl Ove Hufthammer"
msgstr "Karl Ove Hufthammer"

#: ../contributors.pl:45
#, c-format
msgid "Norwegian Nynorsk (nn) translator and coordinator"
msgstr "مترجم و منسّق للترجمة النرويجية النورسك"

#: ../contributors.pl:46
#, c-format
msgid "Marek Laane"
msgstr "Marek Laane"

#: ../contributors.pl:46
#, c-format
msgid "Estonian translator"
msgstr "مترجم الإستونية"

#: ../contributors.pl:47
#, c-format
msgid "Andrea Celli"
msgstr "Andrea Celli"

#: ../contributors.pl:47 ../contributors.pl:48 ../contributors.pl:49
#, c-format
msgid "Italian Translator"
msgstr "مترجم الإيطالية"

#: ../contributors.pl:48 ../contributors.pl:64
#, c-format
msgid "Simone Riccio"
msgstr "Simone Riccio"

#: ../contributors.pl:49 ../contributors.pl:65
#, c-format
msgid "Daniele Pighin"
msgstr "Daniele Pighin"

#: ../contributors.pl:50 ../contributors.pl:68
#, c-format
msgid "Vedran Ljubovic"
msgstr "Vedran Ljubovic"

#: ../contributors.pl:50
#, c-format
msgid "Bosnian translator"
msgstr "مترجم البوسنية"

#: ../contributors.pl:51
#, c-format
msgid "Testers"
msgstr "المختبرون"

#: ../contributors.pl:52
#, c-format
msgid "Benoit Audouard"
msgstr "Benoit Audouard"

#: ../contributors.pl:52
#, c-format
msgid "testing and bug reporting, integration of eagle-usb driver"
msgstr "الاختبار وتقرير الخلل، وتضمين برنامج تعريف eagle-usb"

#: ../contributors.pl:53
#, c-format
msgid "Bernhard Gruen"
msgstr "Bernhard Gruen"

#: ../contributors.pl:53 ../contributors.pl:54 ../contributors.pl:55
#: ../contributors.pl:56 ../contributors.pl:57 ../contributors.pl:58
#: ../contributors.pl:59 ../contributors.pl:60 ../contributors.pl:61
#: ../contributors.pl:62
#, c-format
msgid "testing and bug reporting"
msgstr "الاختبار وتقرير الخلل"

#: ../contributors.pl:54
#, c-format
msgid "Jure Repinc"
msgstr "Jure Repinc"

#: ../contributors.pl:55
#, c-format
msgid "Felix Miata"
msgstr "Felix Miata"

#: ../contributors.pl:56
#, c-format
msgid "Tim Sawchuck"
msgstr "Tim Sawchuck"

#: ../contributors.pl:57
#, c-format
msgid "Eric Fernandez"
msgstr "Eric Fernandez"

#: ../contributors.pl:58
#, c-format
msgid "Ricky Ng-Adam"
msgstr "Ricky Ng-Adam"

#: ../contributors.pl:59
#, c-format
msgid "Pierre Jarillon"
msgstr "Pierre Jarillon"

#: ../contributors.pl:60
#, c-format
msgid "Michael Brower"
msgstr "Michael Brower"

#: ../contributors.pl:61
#, c-format
msgid "Frederik Himpe"
msgstr "Frederik Himpe"

#: ../contributors.pl:62
#, c-format
msgid "Jason Komar"
msgstr "Jason Komar"

#: ../contributors.pl:63
#, c-format
msgid "Raphael Gertz"
msgstr "Raphael Gertz"

#: ../contributors.pl:63
#, c-format
msgid "testing, bug report, Nvidia package try"
msgstr "الاختبار، تقرير الخلل، محاولة إنشاء حزمة Nvidia"

#: ../contributors.pl:64 ../contributors.pl:65 ../contributors.pl:66
#: ../contributors.pl:67 ../contributors.pl:68 ../contributors.pl:69
#, c-format
msgid "testing, bug reporting"
msgstr "الاختبار، وتقرير الخلل"

#: ../contributors.pl:66
#, c-format
msgid "Fabrice FACORAT"
msgstr "Fabrice FACORAT"

#: ../contributors.pl:67
#, c-format
msgid "Mihai Dobrescu"
msgstr "Mihai Dobrescu"

#: ../contributors.pl:69
#, c-format
msgid "Mary V. Jones-Giampalo"
msgstr "Mary V. Jones-Giampalo"

#: ../contributors.pl:70
#, c-format
msgid "Vincent Meyer"
msgstr "Vincent Meyer"

#: ../contributors.pl:70
#, c-format
msgid "MD, testing, bug reporting"
msgstr "MD، الاختبار وتقرير الخلل"

#: ../contributors.pl:71
#, c-format
msgid ""
"And many unnamed and unknown beta testers and bug reporters that helped make "
"sure it all worked right."
msgstr ""
"و العديد من مختبري بيتا و مقرّري الخلل الغير معروفين و الذين ساعدوا في التحقق "
"من أن كلّ شيء عمل جيداً."

#: ../control-center:99 ../control-center:106 ../control-center:1876
#, c-format
msgid "Mandriva Linux Control Center"
msgstr "مركز تحكّم ماندريبا لينكس"

#: ../control-center:109 ../control-center:1471
#, c-format
msgid "Loading... Please wait"
msgstr "التحميل.... الرجاء الانتظار"

#. -PO: this message is already translated in drakx domain from which MCC will searchs it:
#: ../control-center:144 ../control-center:776
#, c-format
msgid "Authentication"
msgstr "المصادقة"

#: ../control-center:145
#, c-format
msgid "Select the authentication method (local, NIS, LDAP, Windows Domain, ...)"
msgstr "اختيار طريقة المصادقة (محلّي، NIS، LDAP، نطاق ويندوز، ...)"

#: ../control-center:155
#, c-format
msgid "Auto Install floppy"
msgstr "قرص مرن للتثبيت الآلي"

#: ../control-center:156
#, c-format
msgid "Generate an Auto Install floppy"
msgstr "توليد قرص مرن للتثبيت الآلي"

#: ../control-center:166
#, c-format
msgid "Autologin"
msgstr "دخول آلي"

#: ../control-center:167
#, c-format
msgid "Enable autologin and select the user to automatically log in"
msgstr "تمكين تسجيل الدخول التلقائي واختيار المستخدم الذي سيسجّل دخوله تلقائيّاً"

#: ../control-center:176
#, c-format
msgid "Backups"
msgstr "النسخ الاحتياطية"

#: ../control-center:177
#, c-format
msgid "Configure backups of the system and of the users' data"
msgstr "تهيئة النسخ الاحتياطيّة من النظام وبيانات المستخدمين"

#: ../control-center:186 ../drakxconf:32
#, c-format
msgid "Boot loader"
msgstr "محمّل الإقلاع"

#: ../control-center:187
#, c-format
msgid "Set up how the system boots"
msgstr "إعداد كيفيّة إقلاع النظام"

#: ../control-center:196
#, c-format
msgid "Boot theme"
msgstr "سمة الإقلاع"

#: ../control-center:197
#, c-format
msgid "Select the graphical theme of the system while booting"
msgstr "اختيار سمة النظام الرسوميّة أثناء الإقلاع"

#: ../control-center:206
#, c-format
msgid "Boot floppy"
msgstr "قرص الإقلاع المرن"

#: ../control-center:207
#, c-format
msgid "Generate a standalone boot floppy"
msgstr "توليد قرص إقلاع مرن قائم ذاتيّاً"

#: ../control-center:216 ../drakxconf:34
#, c-format
msgid "Internet connection sharing"
msgstr "مشاركة اتصال الإنترنت"

#: ../control-center:217
#, c-format
msgid "Share the Internet connection with other local machines"
msgstr "مشاركة اتّصال الإنترنت مع أجهزةٍ محليةٍ أخرى"

#: ../control-center:226
#, c-format
msgid "New connection"
msgstr "وصلة جديدة"

#: ../control-center:227
#, c-format
msgid "Set up a new network interface (LAN, ISDN, ADSL, ...)"
msgstr "إعداد واجهة شبكة جديدة (LAN، ISDN، ADSL، ...)"

#: ../control-center:236
#, c-format
msgid "Internet access"
msgstr "الوصول إلى الإنترنت"

#: ../control-center:237
#, c-format
msgid "Alter miscellaneous internet settings"
msgstr "تعديل إعدادات متفرّقة للإنترنت"

#: ../control-center:246
#, c-format
msgid "Console"
msgstr "لوحة الأوامر"

#: ../control-center:247
#, c-format
msgid "Open a console"
msgstr "فتح لوحة الأوامر"

#: ../control-center:257
#, c-format
msgid "Date and time"
msgstr "التاريخ والوقت"

#: ../control-center:258
#, c-format
msgid "Adjust the date and the time"
msgstr "ضبط التاريخ والوقت"

#: ../control-center:267
#, c-format
msgid "Display manager"
msgstr "مدير العرض"

#: ../control-center:268
#, c-format
msgid "Choose the display manager that enables to select which user to log in"
msgstr "اختيار مدير العرض الذي سيتيح اختيار المستخدم الذي سيسجّل دخوله"

#: ../control-center:288
#, c-format
msgid "Fax"
msgstr "فاكس"

#: ../control-center:289
#, c-format
msgid "Configure a fax server"
msgstr "تهيئة خادم فاكس"

#: ../control-center:298 ../drakxconf:31
#, c-format
msgid "Firewall"
msgstr "الجدار الناري"

#: ../control-center:299
#, c-format
msgid "Set up a personal firewall in order to protect the computer and the network"
msgstr "تهيئة جدار ناري شخصي لحماية الحاسب والشّبكة"

#: ../control-center:308
#, c-format
msgid "Fonts"
msgstr "الخطوط"

#: ../control-center:309
#, c-format
msgid "Manage, add and remove fonts. Import Windows(TM) fonts"
msgstr "إدارة وإضافة وحذف الخطوط. استيراد خطوط ويندوز(م.م.)"

#: ../control-center:318
#, c-format
msgid "Graphical server"
msgstr "خادم رسومي (تخطيطي)"

#: ../control-center:319
#, c-format
msgid "Set up the graphical server"
msgstr "إعداد الخادم الرّسومي"

#: ../control-center:328 ../drakxconf:35
#, c-format
msgid "Partitions"
msgstr "التّجزيئات"

#: ../control-center:329
#, c-format
msgid "Create, delete and resize hard disk partitions"
msgstr "إنشاء وحذف وتحجيم تجزيئات القرص"

#: ../control-center:338 ../control-center:839
#, c-format
msgid "Hardware"
msgstr "العتاد"

#: ../control-center:339
#, c-format
msgid "Look at and configure the hardware"
msgstr "استعراض وتهيئة العتاد"

#: ../control-center:349
#, c-format
msgid "Install"
msgstr "تثبيت"

#: ../control-center:350
#, c-format
msgid "Look at installable software and install software packages"
msgstr "استعراض البرمجيّات المتاح تثبيتها وتثبيت الحزم البرمجيّة"

#: ../control-center:360
#, c-format
msgid "Installed Software"
msgstr "البرامج المثبتة"

#: ../control-center:371 ../drakxconf:26
#, c-format
msgid "Keyboard"
msgstr "لوحة المفاتيح"

#: ../control-center:372
#, c-format
msgid "Set up the keyboard layout"
msgstr "تهيئة مخطّط لوحة المفاتيح"

#: ../control-center:381
#, c-format
msgid "Kolab"
msgstr "Kolab"

#: ../control-center:382
#, c-format
msgid "Set up a groupware server"
msgstr "إعداد خادم لمجموعات العمل"

#. -PO: this message is already translated in drakx domain from which MCC will searchs it:
#: ../control-center:393
#, c-format
msgid "Language"
msgstr "اللغة"

#. -PO: this message is already translated in drakx domain from which MCC will searchs it:
#: ../control-center:395
#, c-format
msgid "Country / Region"
msgstr "الدولة / الإقليم"

#: ../control-center:396
#, c-format
msgid "Select the language and the country or region of the system"
msgstr "اختيار لغة وبلد أو منطقة النّظام"

#: ../control-center:404
#, c-format
msgid "Logs"
msgstr "السجلات"

#: ../control-center:405
#, c-format
msgid "View and search system logs"
msgstr "استعراض وبحث سجلات النّظام"

#: ../control-center:414
#, c-format
msgid "Manage connections"
msgstr "إدارة الوصلات"

#: ../control-center:415
#, c-format
msgid "Reconfigure a network interface"
msgstr "إعادة تهيئة واجهة شبكة"

#: ../control-center:424
#, c-format
msgid "Mandriva Online"
msgstr "Mandriva Online"

#: ../control-center:425
#, c-format
msgid ""
"Upload your configuration in order to keep you informed about security "
"updates and useful upgrades"
msgstr "تحميل تهيئتك كي تبقى على اطلاع حول تحديثات الأمن والترقيات المفيدة"

#: ../control-center:435
#, c-format
msgid "Manage computer group"
msgstr "إدارة مجموعة حاسبات"

#: ../control-center:436
#, c-format
msgid "Manage installed software packages on a group of computers"
msgstr "إدارة الحزم البرمجيّة المثبّتة على مجموعة حاسبات"

#: ../control-center:446
#, c-format
msgid "Updates"
msgstr "التحديثات"

#: ../control-center:447
#, c-format
msgid ""
"Look at available updates and apply any fixes or upgrades to installed "
"packages"
msgstr "استعراض التحديثات المتاحة وتطبيق أيّة إصلاحات أوترقيات على الحزم المثبّتة"

#: ../control-center:457
#, c-format
msgid "Menus"
msgstr "القوائم"

#: ../control-center:458
#, c-format
msgid ""
"Select the application menu layout and change which programs are shown on "
"the menu"
msgstr "اختيار مخطّط قائمة التّطبيقات وتغيير أيّ البرامج ستظهر على القائمة"

#: ../control-center:467
#, c-format
msgid "Monitor"
msgstr "الشاشة"

#: ../control-center:468
#, c-format
msgid "Configure your monitor"
msgstr "تهيئة شاشتك"

#: ../control-center:477
#, c-format
msgid "Monitor connections"
msgstr "مراقبة الوصلات"

#: ../control-center:478
#, c-format
msgid "Monitor the network connections"
msgstr "مراقبة اتّصالات الشّبكة"

#: ../control-center:487 ../drakxconf:27
#, c-format
msgid "Mouse"
msgstr "الماوس"

#: ../control-center:488
#, c-format
msgid "Set up the pointer device (mouse, touchpad)"
msgstr "تهيئة جهاز المؤشّر (الماوس، لوحة اللّمس)"

#: ../control-center:497
#, c-format
msgid "NFS mount points"
msgstr "أماكن تركيب NFS"

#: ../control-center:498
#, c-format
msgid "Set NFS mount points"
msgstr "تعيين أماكن تركيب NFS"

#: ../control-center:507
#, c-format
msgid "Package Stats"
msgstr "أوضاع الحزم"

#: ../control-center:508
#, c-format
msgid "Show statistics about usage of installed software packages"
msgstr "إظهار الإحصائيّات حول استخدام الحزم البرمجيّة المثبّتة"

#: ../control-center:517
#, c-format
msgid "Local disk sharing"
msgstr "تقسيم القرص المحلي"

#: ../control-center:518
#, c-format
msgid "Set up sharing of your hard disk partitions"
msgstr "إعداد مشاركة تجزيئات القرص الصلب"

#: ../control-center:527
#, c-format
msgid "Printers"
msgstr "الطابعات"

#: ../control-center:529
#, c-format
msgid "Set up the printer(s), the print job queues, ..."
msgstr "إعداد الطّابعات، طوابير مهام الطّباعة، ..."

#: ../control-center:538
#, c-format
msgid "Scheduled tasks"
msgstr "المهام المجدولة"

#: ../control-center:539
#, c-format
msgid "Schedule programs to run periodically or at given times"
msgstr "جدولة البرامج لتنفّذ دوريّاً أو في أوقاتٍ معيّنة"

#: ../control-center:548
#, c-format
msgid "Proxy"
msgstr "البروكسي"

#: ../control-center:549
#, c-format
msgid "Set up a proxy server for files and web browsing"
msgstr "إعداد بروكسي للملفّات ولتصفّح الوب"

#: ../control-center:557
#, c-format
msgid "Remote Control (Linux/Unix, Windows)"
msgstr "التحكم عن بعد (لينكس/يونكس، ويندوز)"

#: ../control-center:558
#, c-format
msgid "Remote Control of another machine (Linux/Unix, Windows)"
msgstr "التحكم بجهاز آخر (لينكس/يونكس، ويندوز) عن بعد"

#: ../control-center:567
#, c-format
msgid "Remove a connection"
msgstr "حذف وصلة"

#: ../control-center:568
#, c-format
msgid "Delete a network interface"
msgstr "حذف واجهة شبكة"

#: ../control-center:577
#, c-format
msgid "Remove"
msgstr "حذف"

#: ../control-center:578
#, c-format
msgid "Look at installed software and uninstall software packages"
msgstr "الاطّلاع على البرمجيّات المثبّتة وحذف حزم برمجيّة"

#: ../control-center:588
#, c-format
msgid "Screen resolution"
msgstr "استبانة الشّاشة"

#: ../control-center:589
#, c-format
msgid "Change the screen resolution"
msgstr "تغيير استبانة الشّاشة"

#: ../control-center:598
#, c-format
msgid "Samba mount points"
msgstr "نقاط تركيب Samba"

#: ../control-center:599
#, c-format
msgid "Set Samba mount points"
msgstr "تعيين نقاط تركيب Samba"

#: ../control-center:608
#, c-format
msgid "Scanners"
msgstr "الماسحات الضوئية"

#: ../control-center:609
#, c-format
msgid "Set up scanner"
msgstr "إعداد الماسح الضوئي"

#: ../control-center:618
#, c-format
msgid "Level and checks"
msgstr "المستوى والفحوصات"

#: ../control-center:619
#, c-format
msgid "Set the system security level and the periodic security audit"
msgstr "تعيين مستوى أمن النظام والمراجعة الأمنيّة الدوريّة"

#: ../control-center:629
#, c-format
msgid "Permissions"
msgstr "السماحيات"

#: ../control-center:630
#, c-format
msgid "Fine-tune the security permissions of the system"
msgstr "ضبط تصاريح أمن النّظام"

#: ../control-center:640 ../drakxconf:30
#, c-format
msgid "Services"
msgstr "الخدمات"

#: ../control-center:641
#, c-format
msgid "Enable or disable the system services"
msgstr "تمكين أو تعطيل خدمات النّظام"

#: ../control-center:650
#, c-format
msgid "Media Manager"
msgstr "مدير الوسائط"

#: ../control-center:651
#, c-format
msgid "Select from where software packages are downloaded when updating the system"
msgstr "اختر المكان الذي ستقوم بتنزيل الحزم البرمجيّة منه عند تحديث النظام"

#: ../control-center:660
#, c-format
msgid "TV card"
msgstr "بطاقة التلفاز"

#: ../control-center:661
#, c-format
msgid "Set up TV card"
msgstr "إعداد بطاقة التلفاز"

#: ../control-center:670
#, c-format
msgid "UPS"
msgstr "UPS"

#. -PO: here power means electrical power
#: ../control-center:673
#, c-format
msgid "Set up a UPS for power monitoring"
msgstr "إعداد UPS لمراقبة الطّاقة"

#: ../control-center:683 ../drakxconf:29
#, c-format
msgid "Users and groups"
msgstr "المستخدمون والمجموعات"

#: ../control-center:684
#, c-format
msgid "Add, remove or change users of the system"
msgstr "إضافة أو حذف أو تغيير مستخدمي النّظام"

#: ../control-center:694
#, c-format
msgid "WebDAV mount points"
msgstr "أماكن تركيب WebDAV"

#: ../control-center:695
#, c-format
msgid "Set WebDAV mount points"
msgstr "تعيين أماكن تركيب WebDAV"

#: ../control-center:721
#, c-format
msgid "Software Management"
msgstr "إدارة البرامج"

#: ../control-center:737
#, c-format
msgid "Server wizards"
msgstr "مرشدات الخوادم"

#: ../control-center:738
#, c-format
msgid "Sharing"
msgstr "المشاركة"

#: ../control-center:741
#, c-format
msgid "Configure FTP"
msgstr "تهيئة FTP"

#: ../control-center:742
#, c-format
msgid "Set up an FTP server"
msgstr "إعداد خادم FTP"

#: ../control-center:744
#, c-format
msgid "Configure Samba"
msgstr "تهيئة Samba"

#: ../control-center:745
#, c-format
msgid ""
"Set up a file and print server for workstations running Linux and non-Linux "
"systems"
msgstr "إعداد خادم ملفّات وطباعة لمحطّات عمل أنظمة لينكس وغيرها"

#: ../control-center:747
#, c-format
msgid "Configure web server"
msgstr "تهيئة خادم الوب"

#: ../control-center:748
#, c-format
msgid "Set up a web server"
msgstr "تهيئة خادم الوب"

#: ../control-center:750
#, c-format
msgid "Configure installation server"
msgstr "تهيئة خادم التثبيت"

#: ../control-center:751
#, c-format
msgid "Set up server for network installations of Mandriva Linux"
msgstr "إعداد خادم لتثبيتات ماندريبا لينكس الشبكيّة"

#: ../control-center:757
#, c-format
msgid "Network Services"
msgstr "خدمات الشبكة"

#: ../control-center:760
#, c-format
msgid "Configure DHCP"
msgstr "تهيئة DHCP"

#: ../control-center:761
#, c-format
msgid "Set up a DHCP server"
msgstr "إعداد خادم DHCP"

#: ../control-center:763
#, c-format
msgid "Configure DNS"
msgstr "تهيئة DNS"

#: ../control-center:764
#, c-format
msgid "Set up a DNS server (network name resolution)"
msgstr "إعداد خادم DNS (استخلاص اسم الشّبكة)"

#: ../control-center:766
#, c-format
msgid "Configure proxy"
msgstr "تهيئة البروكسي"

#: ../control-center:767
#, c-format
msgid "Configure a web caching proxy server"
msgstr "تهيئة خادم بروكسي مخبّئ للوب"

#: ../control-center:769
#, c-format
msgid "Configure time"
msgstr "تهيئة الوقت"

#: ../control-center:770
#, c-format
msgid "Set the time of the server to be synchronized with an external time server"
msgstr "تعيين وقت الخادم الذي سيزامن مع خادم زمني خارجي"

#: ../control-center:780
#, c-format
msgid "Configure NIS and Autofs"
msgstr "تهيئة NIS و Autofs"

#: ../control-center:781
#, c-format
msgid "Configure the NIS and Autofs services"
msgstr "تهيئة خدمات NIS و Autofs"

#: ../control-center:783
#, c-format
msgid "Configure LDAP"
msgstr "تهيئة LDAP"

#: ../control-center:784
#, c-format
msgid "Configure the LDAP directory services"
msgstr "تهيئة خدمات دليل LDAP"

#: ../control-center:790
#, c-format
msgid "Groupware"
msgstr "برامج المجموعات"

#: ../control-center:793
#, c-format
msgid "Configure news"
msgstr "تهيئة الأخبار"

#: ../control-center:794
#, c-format
msgid "Configure a newsgroup server"
msgstr "تهيئة خادم مجموعات أخبار"

#: ../control-center:796
#, c-format
msgid "Configure groupware"
msgstr "تهيئة برامج المجموعات"

#: ../control-center:797
#, c-format
msgid "Configure a groupware server"
msgstr "تهيئة خادم برامج مجموعات"

#: ../control-center:799
#, c-format
msgid "Configure mail"
msgstr "تهيئة البريد"

#: ../control-center:800
#, c-format
msgid "Configure the Internet Mail services"
msgstr "تهيئة خدمات بريد الإنترنت"

#: ../control-center:809
#, c-format
msgid "Online Administration"
msgstr "إدارة على الخطّ"

#: ../control-center:825
#, c-format
msgid "Local administration"
msgstr "إدارة محليّة"

#: ../control-center:826
#, c-format
msgid "Configure the local machine via web interface"
msgstr "تهيئة الجهاز المحلّي عبر واجهة وب"

#: ../control-center:826
#, c-format
msgid "You don't seem to have webmin intalled. Local config is disabled"
msgstr "لا يبدو أن webmin مثبّتٌ لديك. عطّلت التهيئة المحليّة"

#: ../control-center:828
#, c-format
msgid "Remote administration"
msgstr "الإدارة عن بعد"

#: ../control-center:829
#, c-format
msgid "Click here if you want to configure a remote box via Web interface"
msgstr "اضغط هنا إن كنت تريد تهيئة جهازٍ بعيد عبر واجهة وب"

#: ../control-center:855 ../drakxconf:28
#, c-format
msgid "Network & Internet"
msgstr "الشبكة والإنترنت"

#: ../control-center:868
#, c-format
msgid "System"
msgstr "النظام"

#: ../control-center:884
#, c-format
msgid "Mount Points"
msgstr "أماكن التركيب"

#: ../control-center:906
#, c-format
msgid "CD-ROM"
msgstr "CD-ROM"

#: ../control-center:907
#, c-format
msgid "Set where your CD-ROM drive is mounted"
msgstr "تعيين مكان تركيب القرص المدمج"

#: ../control-center:909
#, c-format
msgid "DVD-ROM"
msgstr "DVD-ROM"

#: ../control-center:910
#, c-format
msgid "Set where your DVD-ROM drive is mounted"
msgstr "تعيين مكان تركيب سوّاقة DVD-ROM"

#: ../control-center:912
#, c-format
msgid "CD/DVD burner"
msgstr "ناسخة CD/DVD"

#: ../control-center:913
#, c-format
msgid "Set where your CD/DVD burner is mounted"
msgstr "تعيين مكان تركيب جهاز نسخ CD/DVD"

#: ../control-center:915
#, c-format
msgid "Floppy drive"
msgstr "سوّاقة الأقراص المرنة"

#: ../control-center:916
#, c-format
msgid "Set where your floppy drive is mounted"
msgstr "تعيين مكان تركيب القرص المرن"

#: ../control-center:918
#, c-format
msgid "ZIP drive"
msgstr "سوّاقة ZIP"

#: ../control-center:919
#, c-format
msgid "Set where your ZIP drive is mounted"
msgstr "تعيين مكان تركيب قرص ZIP"

#: ../control-center:930
#, c-format
msgid "Security"
msgstr "الأمن"

#: ../control-center:938
#, c-format
msgid "Boot"
msgstr "الإقلاع"

#: ../control-center:951
#, c-format
msgid "Additional wizards"
msgstr "مرشدات إضافيّة"

#: ../control-center:1001 ../control-center:1002 ../control-center:1003
#: ../control-center:1004 ../control-center:1024
#, c-format
msgid "/_Options"
msgstr "/خ_يارات"

#: ../control-center:1001
#, c-format
msgid "/Display _Logs"
msgstr "/عرض ال_سجلات"

#: ../control-center:1002
#, c-format
msgid "/_Embedded Mode"
msgstr "/_وضع مدمج"

#: ../control-center:1003
#, c-format
msgid "/Expert mode in _wizards"
msgstr "/وضع الخبرة في المر_شدات"

#: ../control-center:1008
#, c-format
msgid "/_Profiles"
msgstr "/ال_سجلات الشخصية"

#: ../control-center:1009
#, c-format
msgid "/_Delete"
msgstr "/_حذف"

#: ../control-center:1010
#, c-format
msgid "/_New"
msgstr "/_جديد"

#: ../control-center:1022 ../control-center:1023
#, c-format
msgid "/_File"
msgstr "/_ملف"

#: ../control-center:1023
#, c-format
msgid "/_Quit"
msgstr "/_خروج"

#: ../control-center:1023
#, c-format
msgid "<control>Q"
msgstr "<control>Q"

#: ../control-center:1023
#, c-format
msgid "Quit"
msgstr "خروج"

#: ../control-center:1057 ../control-center:1060 ../control-center:1073
#, c-format
msgid "/_Themes"
msgstr "/_سمات"

#: ../control-center:1063
#, c-format
msgid ""
"This action will restart the control center.\n"
"Any change not applied will be lost."
msgstr ""
"هذا الفعل سوف يعيد تشغيل مركز التحكم.\n"
"سوف تضيع أية تغييرات لم يتم تطبيقها."

#: ../control-center:1073
#, c-format
msgid "/_More themes"
msgstr "/_سمات أكثر "

#: ../control-center:1077
#, c-format
msgid "New profile..."
msgstr "سجل شخصي جديد..."

#: ../control-center:1080
#, c-format
msgid ""
"Name of the profile to create (the new profile is created as a copy of the "
"current one):"
msgstr "إسم السجل الشخصي الواجب إنشاؤه (سيتمّ إنشاء السجل الشخصي الجديد كنسخة للحالي):"

#: ../control-center:1084 ../control-center:1117 ../control-center:1224
#, c-format
msgid "Cancel"
msgstr "إلغاء"

#: ../control-center:1086 ../control-center:1118
#, c-format
msgid "Ok"
msgstr "موافق"

#: ../control-center:1092 ../control-center:1450 ../control-center:1517
#, c-format
msgid "Error"
msgstr "خطأ"

#: ../control-center:1092
#, c-format
msgid "The \"%s\" profile already exists!"
msgstr "السجل الشخصي \"%s\" موجود !"

#: ../control-center:1110
#, c-format
msgid "Delete profile"
msgstr "حذف السجل الشخصي"

#: ../control-center:1112
#, c-format
msgid "Profile to delete:"
msgstr "السجل الشخصي المطلوب حذفه:"

#: ../control-center:1121 ../control-center:1179 ../control-center:1778
#, c-format
msgid "Warning"
msgstr "تحذير"

#: ../control-center:1121
#, c-format
msgid "You can not delete the current profile"
msgstr "لا يمكنك حذف السجل الشخصي الحالي"

#: ../control-center:1136 ../control-center:1137 ../control-center:1138
#: ../control-center:1139
#, c-format
msgid "/_Help"
msgstr "/_مساعدة"

#: ../control-center:1137
#, c-format
msgid "Help"
msgstr "مساعدة"

#: ../control-center:1138
#, c-format
msgid "/_Report Bug"
msgstr "/_تقرير خطأ"

#: ../control-center:1139
#, c-format
msgid "/_About..."
msgstr "/_حول..."

#: ../control-center:1180
#, c-format
msgid ""
"We are about to switch from the \"%s\" profile to the \"%s\" profile.\n"
"\n"
"Are you sure you want to do the switch?"
msgstr ""
"نحن بصدد التبديل من السجل الشخصي \"%s\" إلى السجل الشخصي \"%s\".\n"
"\n"
"هل تريد التّبديل فعلاً؟"

#: ../control-center:1258
#, c-format
msgid "Mandriva Linux Control Center %s [on %s]"
msgstr "مركز تحكّم ماندريبا لينكس %s [على %s]"

#: ../control-center:1272
#, c-format
msgid "Welcome to the Mandriva Linux Control Center"
msgstr "أهلاً بك في مركز تحكم ماندريبا لينكس"

#: ../control-center:1450
#, c-format
msgid ""
"There's a bug in translations of your language (%s)\n"
"\n"
"Please report that bug."
msgstr ""
"هناك علّة في التّرجمات الخاصة بلغتك (%s)\n"
"\n"
"الرجاء الإبلاغ بتلك العلّة."

#: ../control-center:1517
#, c-format
msgid "Impossible to run unknown '%s' program"
msgstr "من المستحيل تشغيل البرنامج '%s' المجهول"

#: ../control-center:1536
#, c-format
msgid "The modifications done in the current module won't be saved."
msgstr "حفظت التعديلات في الوحدة الحاليّة"

#: ../control-center:1629
#, c-format
msgid "cannot fork: %s"
msgstr "تعذر عمل fork: %s"

#: ../control-center:1640
#, c-format
msgid "cannot fork and exec \"%s\" since it is not executable"
msgstr "تعذر عمل fork وتنفيذ \"%s\" لأنه غير قابل للتنفيذ"

#: ../control-center:1769
#, c-format
msgid "This program has exited abnormally"
msgstr "خرج هذا البرنامج بطريقة غير عادية"

#: ../control-center:1788 ../drakconsole:31
#, c-format
msgid "Close"
msgstr "إغلاق"

#: ../control-center:1795
#, c-format
msgid "More themes"
msgstr "سمات أكثر"

#: ../control-center:1797
#, c-format
msgid "Getting new themes"
msgstr "إحضار سمات جديدة"

#: ../control-center:1798
#, c-format
msgid "Additional themes"
msgstr "سمات إضافية "

#: ../control-center:1800
#, c-format
msgid "Get additional themes on www.damz.net"
msgstr "احصل على السمات الإضافيّة على www.damz.net"

#: ../control-center:1808
#, c-format
msgid "About - Mandriva Linux Control Center"
msgstr "حول - مركز تحكّم ماندريبا لينكس"

#: ../control-center:1817
#, c-format
msgid "Authors: "
msgstr "المؤلفون: "

#: ../control-center:1818
#, c-format
msgid "(original C version)"
msgstr "(إصدار C الأصلي)"

#: ../control-center:1821 ../control-center:1824
#, c-format
msgid "(perl version)"
msgstr "(إصدار Perl)"

#: ../control-center:1826
#, c-format
msgid "Artwork: "
msgstr "العمل الفني: "

#: ../control-center:1827
#, c-format
msgid "(design)"
msgstr "(التصميم)"

#: ../control-center:1831
#, c-format
msgid "Helene Durosini"
msgstr "Helene Durosini"

#. -PO: this is used as "language: translator" in credits part of the about dialog:
#: ../control-center:1852
#, c-format
msgid "- %s: %s\n"
msgstr "- %s: %s\n"

#: ../control-center:1867
#, c-format
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "Ossama Khayat"

#: ../control-center:1869
#, c-format
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "okhayat@yahoo.com"

#: ../control-center:1871
#, c-format
msgid "Translator: "
msgstr "المترجم: "

#: ../control-center:1878
#, c-format
msgid "Copyright (C) 1999-2005 Mandriva SA"
msgstr "حقوق النسخ 1999-2005 ماندريبا سوفت ش.م."

#: ../control-center:1884
#, c-format
msgid "Authors"
msgstr "المؤلفون"

#: ../control-center:1885
#, c-format
msgid "Mandriva Linux Contributors"
msgstr "مساهمو ماندريبا لينكس"

#: ../drakconsole:27
#, c-format
msgid "DrakConsole"
msgstr "DrakConsole"

#: ../drakxconf:25
#, c-format
msgid "Display"
msgstr "العرض"

#: ../drakxconf:33
#, c-format
msgid "Auto Install"
msgstr "تثبيت آلي"

#: ../drakxconf:38
#, c-format
msgid "Control Center"
msgstr "مركز التحكم"

#: ../drakxconf:38
#, c-format
msgid "Choose the tool you want to use"
msgstr "اختيار الأداة التي تودّ استعمالها"

#: ../menus_launcher.pl:19 ../menus_launcher.pl:41
#, c-format
msgid "Menu Configuration Center"
msgstr "مركز تهيئة القوائم"

#: ../menus_launcher.pl:28
#, c-format
msgid "System menu"
msgstr "قائمة النظام"

#: ../menus_launcher.pl:29 ../menus_launcher.pl:36 ../print_launcher.pl:31
#, c-format
msgid "Configure..."
msgstr "تهيئة..."

#: ../menus_launcher.pl:31
#, c-format
msgid "User menu"
msgstr "قائمة المستخدم"

#: ../menus_launcher.pl:41
#, c-format
msgid ""
"\n"
"\n"
"Choose which menu you want to configure"
msgstr ""
"\n"
"\n"
"اختيار القائمة التي تريد تهيئتها"

#: ../print_launcher.pl:14 ../print_launcher.pl:21
#, c-format
msgid "Printing configuration"
msgstr "تهيئة الطباعة"

#: ../print_launcher.pl:30
#, c-format
msgid "Click here to configure the printing system"
msgstr "اضغط هنا لتهيئة نظام الطباعة"

#: ../print_launcher.pl:37
#, c-format
msgid "Done"
msgstr "تم"

#: ../data/drakboot.desktop.in.h:1
msgid "Boot Loading"
msgstr "تحميل الإقلاع"

#: ../data/drakcronat.desktop.in.h:1
msgid "Programs scheduling"
msgstr "جدولة البرامج"

#: ../data/draksec.desktop.in.h:1
msgid "Levels and Checks"
msgstr "المستويات والفحوصات"

#: ../data/drakxtv.desktop.in.h:1
msgid "TV Cards"
msgstr "بطاقات التلفاز"

#: ../data/fileshare.desktop.in.h:1
msgid "Partition Sharing"
msgstr "مشاركة التجزيء"

#: ../data/harddrive.desktop.in.h:1
msgid "Hard Drives"
msgstr "الأقراص الصلبة"

#: ../data/proxy.desktop.in.h:1
msgid "Proxy Configuration"
msgstr "تهيئة البروكسي"

#: ../data/removable.desktop.in.h:1
msgid "Removable devices"
msgstr "الأجهزة القابلة للإزالة"

#: ../data/remove-connection.desktop.in.h:1
msgid "Remove Connection"
msgstr "حذف الوصلة"

#: ../data/SystemConfig.directory.in.h:1
msgid "System Settings"
msgstr "إعدادات النظام"

#: ../data/userdrake.desktop.in.h:1
msgid "Users and Groups"
msgstr "المستخدمون والمجموعات"

#: ../data/XFDrake-Resolution.desktop.in.h:1
msgid "Screen Resolution"
msgstr "استبانة الشاشة"

